var chai    = require('chai');
var expect  = require('chai').expect;
var chaiXml = require('chai-xml');
const fs = require("fs");

//loads the plugin
chai.use(chaiXml);

describe('assert some xml', function(){
    var inputdir = '/Users/shiny/Documents/Exeter/Packagecreator/tests/inputs';
    var outputdir = '/Users/shiny/Documents/Exeter/Packagecreator/tests/outputs';
    fs.readdir(inputdir, (err, files) => {
        files.forEach(file => {
            inputfilename = inputdir + '/' + file;
            outputfilename = outputdir + '/' + file;
            var actualPath = fs.readFileSync(inputfilename).toString();
            var expectedPath = fs.readFileSync(outputfilename).toString();
            it(file + "\n\tshould be valid", function(){
                expect(expectedPath).xml.to.be.valid();
            });
            it("should be the same XML as otherXml ", function(){
                expect(actualPath).to.equal(actualPath);
            });
        });
    });
});