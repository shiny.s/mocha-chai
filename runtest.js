var Mocha = require('mocha'),
    fs = require('fs'),
    path = require('path');

// Instantiate a Mocha instance.
var mocha = new Mocha({
    reporter: 'mochawesome',
    reporterOptions: {
      reportDir: './cms/reports/mocha',
      reportFilename: 'index.html',
      quiet: true
    },
    timeout: 20000
});
var testDir = '/Users/shiny/Documents/Exeter/Packagecreator/test'

// Add each .js file to the mocha instance
fs.readdirSync(testDir).filter(function(file) {
    // Only keep the .js files
    return file.substr(-7) === 'spec.js';

}).forEach(function(file) {
    mocha.addFile(
        path.join(testDir, file)
    );
});

// Run the tests.
mocha.run(function(failures) {
  process.exitCode = failures ? 1 : 0;  // exit with non-zero status if there were failures
});
